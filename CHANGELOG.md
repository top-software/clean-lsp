# Changelog

### 1.2.0

- Feature: add 'notifcationMessagePublishDiagnostics' utility function for publishing diagnostics.

### 1.1.0

- Feature: add `==` instance for `:: NotificationMessage`.

#### 1.0.3

- Chore: accept `base` `3`.

#### 1.0.2

- Chore: update to base `2` and latest version of other packages; remove dependency on clean-platform.

#### 1.0.1

- Fix: add src/ as src in nitrile.yml to make sure modules are actually packaged.

## 1.0.0

- Broken version.
