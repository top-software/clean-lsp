definition module LSP.InitializeParams

from Text.GenJSON import :: JSONNode
from Text.URI import :: URI
from LSP.Internal.Serialize import generic gLSPJSONEncode, generic gLSPJSONDecode

/**
 * Params for the initialize request.
 *
 * Not all fields are implemented because this would require more boilerplate.
 */
:: InitializeParams =
	{ rootPath         :: !?String             //* Deprecated in favour of `rootUri`.
	, rootUri          :: !?URI                //* Deprecated in favour of `workspaceFolders`.
	, workspaceFolders :: !?[!WorkspaceFolder] //* Workspaces the user has open.
	}

//* Ad hoc type for `InitializeParams`.
:: ClientInfo =
	{ name    :: !String
	, version :: !?String
	}

//* Ad hoc type for `InitializeParams`.
:: WorkspaceFolder =
	{ uri  :: !URI
	, name :: !String
	}

derive gLSPJSONEncode InitializeParams, ClientInfo, WorkspaceFolder
derive gLSPJSONDecode InitializeParams, ClientInfo, WorkspaceFolder
