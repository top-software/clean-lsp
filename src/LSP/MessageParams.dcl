definition module LSP.MessageParams

//* Module for `ShowMessageParams` and `LogMessageParams` which are identical.

from Text.GenJSON import :: JSONNode
from LSP.Internal.Serialize import generic gLSPJSONEncode, generic gLSPJSONDecode

:: MessageParams =
	{ type :: !MessageType
	, message :: !String
	}
derive gLSPJSONEncode MessageParams

:: MessageType = Error | Warning | Info | Log
derive gLSPJSONEncode MessageType
