definition module LSP.RequestId

from Data.Either import :: Either
from LSP.Internal.Serialize import generic gLSPJSONEncode, generic gLSPJSONDecode
from Text.GenJSON import :: JSONNode

//* Uniquely identifies a request, exported for test.
:: RequestId =: RequestId (Either Int String)

derive gLSPJSONEncode RequestId
derive gLSPJSONDecode RequestId
