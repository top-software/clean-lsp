implementation module LSP.ResponseMessage

import StdEnv
import Data.Either
import Data.Func
import Text.GenJSON
import LSP.Internal.Serialize
import LSP.RequestId

derive gLSPJSONEncode ResponseMessage
derive gLSPJSONDecode ResponseMessage

/**
 * `code` is a reserved keyword in Clean, but we still want to create it. So we just gene
 */
gLSPJSONEncode{|ResponseError|} err = fixupErrorCode (gLSPJSONEncode err)
where
	derive gLSPJSONEncode ResponseError

	fixupErrorCode :: ![JSONNode] -> [JSONNode]
	fixupErrorCode js = map fixupErrorCode` js
	where
		fixupErrorCode` :: !JSONNode -> JSONNode
		fixupErrorCode` (JSONObject fields) = JSONObject $ map fixupErrorCode`` fields
		where
			fixupErrorCode`` :: !(String, JSONNode) -> (String, JSONNode)
			fixupErrorCode`` ("errorCode", node) = ("code", node)
			fixupErrorCode`` field = field
		fixupErrorCode` json = json

gLSPJSONDecode{|ResponseError|} [JSONObject fields : xs]
	= gLSPJSONDecode [JSONObject (map fixupErrorCode fields) : xs]
where
	derive gLSPJSONDecode ResponseError

	fixupErrorCode :: !(String, JSONNode) -> (String, JSONNode)
	fixupErrorCode ("code", node) = ("errorCode", node)
	fixupErrorCode field = field

instance toInt ErrorCode
where
	toInt ParseError = -32700
	toInt InvalidRequest = -32600
	toInt MethodNotFound = -32601
	toInt InvalidParams = -32602
	toInt InternalError = -32603
	toInt ServerNotInitialized = -32002
	toInt UnknownErrorCode = -32001
	toInt ContentModified = -32801
	toInt RequestCancelled= -32800

instance fromInt ErrorCode
where
	fromInt -32700 = ParseError
	fromInt -32600 = InvalidRequest
	fromInt -32601 = MethodNotFound
	fromInt -32602 = InvalidParams
	fromInt -32603 = InternalError
	fromInt -32002 = ServerNotInitialized
	fromInt -32801 = ContentModified
	fromInt -32800 = RequestCancelled
	fromInt _ = UnknownErrorCode

gLSPJSONEncode{|ErrorCode|} x = [JSONInt $ toInt x]
gLSPJSONDecode{|ErrorCode|} [JSONInt ec : xs] = (?Just $ fromInt ec, xs)
gLSPJSONDecode{|ErrorCode|} xs = (?None, xs)

responseMessage :: !(?RequestId) !(Either ResponseError a) -> ResponseMessage | gLSPJSONEncode{|*|} a
responseMessage id (Left error) = 
	{ ResponseMessage
	| id = id
	, result = ?None
	, error = ?Just error
	}
responseMessage id (Right res) = 
	{ ResponseMessage
	| id = id
	, result = ?Just (serialize res)
	, error = ?None
	}
