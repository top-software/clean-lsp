definition module LSP.PublishDiagnosticsParams

from Text.GenJSON import :: JSONNode
from Text.URI import :: URI
from LSP.Internal.Serialize import generic gLSPJSONEncode, generic gLSPJSONDecode
from LSP.Diagnostic import :: Diagnostic

//* Params for a diagnostic notification the server sends to the client about a single resource.
:: PublishDiagnosticsParams =
	{ uri         :: !URI           //* The URI of the resource the diagnostics are related to.
	, diagnostics :: ![!Diagnostic] //* The diagnostics of the indicated resource.
	}

derive gLSPJSONEncode PublishDiagnosticsParams
derive gLSPJSONDecode PublishDiagnosticsParams
