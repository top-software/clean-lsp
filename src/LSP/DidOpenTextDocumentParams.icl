implementation module LSP.DidOpenTextDocumentParams

import LSP.Internal.Serialize
import LSP.TextDocumentIdentifier

derive gLSPJSONEncode DidOpenTextDocumentParams
derive gLSPJSONDecode DidOpenTextDocumentParams
