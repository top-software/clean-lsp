definition module LSP.ServerCapabilities

from LSP.Internal.Serialize import generic gLSPJSONEncode, generic gLSPJSONDecode
from Text.GenJSON import :: JSONNode

//* The capabilities of the server.
:: ServerCapabilities =
	{ textDocumentSync :: !TextDocumentSyncOptions //* Indicates the text document capabilities of the server.
	, declarationProvider :: !Bool //* Indicates whether the server supports go to declaration.
	, definitionProvider :: !Bool //* Indicates whether the server supports go to definition.
	}

derive gLSPJSONEncode ServerCapabilities
derive gLSPJSONDecode ServerCapabilities

//* Indicates which text document notifications should be sent to the server.
:: TextDocumentSyncOptions =
	{ openClose :: !Bool //* If set open and close notifications are sent to the server.
	, save :: !Bool      //* If set save notifications are sent to the server.
	}
