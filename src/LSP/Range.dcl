definition module LSP.Range

from Text.GenJSON import :: JSONNode
from LSP.Internal.Serialize import generic gLSPJSONEncode, generic gLSPJSONDecode
from LSP.Position import :: Position

:: Range =
	{ start :: !Position
	, end :: !Position
	}

derive gLSPJSONEncode Range
derive gLSPJSONDecode Range
