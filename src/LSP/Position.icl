implementation module LSP.Position

import LSP.Internal.Serialize
import LSP.BasicTypes

derive gLSPJSONEncode Position
derive gLSPJSONDecode Position
