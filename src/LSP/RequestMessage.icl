implementation module LSP.RequestMessage

import LSP.Internal.Serialize
import LSP.RequestId

derive gLSPJSONEncode RequestMessage
derive gLSPJSONDecode RequestMessage
