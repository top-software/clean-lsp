definition module LSP.Position

from Text.GenJSON import :: JSONNode
from LSP.Internal.Serialize import generic gLSPJSONEncode, generic gLSPJSONDecode
from LSP.BasicTypes import :: UInt

:: Position =
	{ line :: !UInt
	, character :: !UInt
	}

derive gLSPJSONEncode Position
derive gLSPJSONDecode Position
