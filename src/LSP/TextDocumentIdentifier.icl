implementation module LSP.TextDocumentIdentifier

import Text.URI
import LSP.Internal.Serialize

derive gLSPJSONEncode TextDocumentIdentifier
derive gLSPJSONDecode TextDocumentIdentifier
