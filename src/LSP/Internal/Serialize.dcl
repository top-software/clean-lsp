definition module LSP.Internal.Serialize

from Data.Either import :: Either
from Text.GenJSON import :: JSONNode
from Text.URI import :: URI
import StdGeneric

/**
 * We have to generate JSON that is compatible with the JSON expected by the LSP. For this reason we do not use the
 * GenJSON generics.
 *
 * Eventually, this module could be added to Platform as a TypeScript-compatible JSON serializer/deserializer.
 */
generic gLSPJSONEncode a :: !a -> [JSONNode]
generic gLSPJSONDecode a :: ![JSONNode] -> (?a, [JSONNode])

// Basic Types
derive gLSPJSONEncode Int, Real, Char, Bool, String
derive gLSPJSONDecode Int, Real, Char, Bool, String

// Generic Types
derive gLSPJSONEncode UNIT, EITHER, OBJECT, CONS, PAIR, RECORD of {grd_fields}, FIELD
derive gLSPJSONDecode UNIT, EITHER, OBJECT, CONS of {gcd_name}, PAIR, RECORD, FIELD of {gfd_name}

// Special Types
derive gLSPJSONEncode ?, Either, JSONNode, [!], [!!]
derive gLSPJSONDecode ?, Either, JSONNode, [!], [!!]

derive gLSPJSONEncode URI
derive gLSPJSONDecode URI

serialize :: !a -> JSONNode | gLSPJSONEncode{|*|} a
deserialize :: !JSONNode -> a | gLSPJSONDecode{|*|} a
