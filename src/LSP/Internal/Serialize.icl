implementation module LSP.Internal.Serialize

from Data.List import lookup
import Data.Either, Data.Func
import StdEnv, StdMaybe
import Text.GenJSON, Text.URI

/**
 * Note [No Constructors]
 *
 * Constructors are not in JSON, we do not want to encode them. During decoding, try the alternatives. Standard enum
 * are typically encoded to integers. Since we have no knowledge on them however, these will have to be derived
 * seperately.
 *
 * This has consequences for constructors that take the same types as other constructors in the same type. An `Either
 * Int Int` will always favour `Left x` over `Right x` during decoding.
 */

generic gLSPJSONEncode a :: !a -> [JSONNode]
generic gLSPJSONDecode a :: ![JSONNode] -> (?a, [JSONNode])

gLSPJSONEncode{|Int|} x = [JSONInt x]
gLSPJSONEncode{|Real|} x = [JSONReal x]
gLSPJSONEncode{|Char|} x = [JSONString {x}]
gLSPJSONEncode{|Bool|} x = [JSONBool x]
gLSPJSONEncode{|String|} x = [JSONString x]

gLSPJSONDecode{|Int|} [JSONInt i:xs] = (?Just i, xs)
gLSPJSONDecode{|Int|} l = (?None, l)
gLSPJSONDecode{|Real|} [JSONReal r:xs] = (?Just r, xs)
gLSPJSONDecode{|Real|} [JSONInt i:xs] = (?Just (toReal i), xs)
gLSPJSONDecode{|Real|} l = (?None, l)
gLSPJSONDecode{|Char|} l=:[JSONString s=:{[0]=c}:xs]
	| size s == 1 = (?Just c, xs)
	| otherwise = (?None, l)
gLSPJSONDecode{|Char|} l = (?None, l)
gLSPJSONDecode{|Bool|} [JSONBool b:xs] = (?Just b, xs)
gLSPJSONDecode{|Bool|} l = (?None, l)
gLSPJSONDecode{|String|} [JSONString s:xs] = (?Just s, xs)
gLSPJSONDecode{|String|} l = (?None, l)

gLSPJSONEncode{|UNIT|} (UNIT) = []
// JSON doesn't support pairs, instead we encode it as an array, which is a typical representation for tuples in TS.
gLSPJSONEncode{|PAIR|} fl fr (PAIR l r) = fl l ++ fr r
gLSPJSONEncode{|EITHER|} fl _ (LEFT l) = fl l
gLSPJSONEncode{|EITHER|} _ fr (RIGHT r) = fr r
gLSPJSONEncode{|OBJECT|} fx (OBJECT x) = fx x
// See Note [No Constructors]
gLSPJSONEncode{|CONS|} fx (CONS x) = fx x
// Null fields are simply removed from the record
gLSPJSONEncode{|RECORD of {grd_fields}|} fx (RECORD x)
	= [JSONObject [(name, field) \\ field <- fx x & name <- grd_fields | isNotNull field]]
where
	isNotNull :: !JSONNode -> Bool
	isNotNull JSONNull = False
	isNotNull _ = True
gLSPJSONEncode{|FIELD|} fx (FIELD x) = fx x

gLSPJSONDecode{|UNIT|} l = (?Just UNIT, l)
// Atempt LEFT first, try RIGHT after. See Note [No Constructors].
gLSPJSONDecode{|EITHER|} fl fr xs = case fl xs of
	(?Just l, xs) -> (?Just (LEFT l), xs)
	_ -> case fr xs of
		(?Just r, xs) -> (?Just (RIGHT r), xs)
		_ -> (?None, xs)
gLSPJSONDecode{|OBJECT|} fx xs = case fx xs of
	(?Just x, xs) -> (?Just (OBJECT x), xs)
	_ -> (?None, xs)
// See Note [No Constructors] as to why we disregard the constructor.
gLSPJSONDecode{|CONS of {gcd_name}|} fx xs = case fx xs of
	(?Just x, xs) -> (?Just (CONS x), xs)
	_ -> (?None, xs)
gLSPJSONDecode{|PAIR|} fl fr xs = case fl xs of
	(?Just l, ls) -> case fr ls of
		(?Just r, rs) -> (?Just (PAIR l r), rs)
		_ -> (?None, xs)
	_ -> (?None, xs)
// For an object, we can simply decode the fields.
gLSPJSONDecode{|RECORD|} fx ls=:[obj : xs] = case fx [obj] of
	(?Just x, _) -> (?Just (RECORD x), xs)
	_ -> (?None, ls)
gLSPJSONDecode{|RECORD|} _ xs = (?None, xs)
// Fields in JSON are not guaranteed to be in order, so we have to find the proper field first.
// If the field is not found we still have to try to decode, as optional values can be omitted.
gLSPJSONDecode{|FIELD of {gfd_name}|} fx xs=:[JSONObject fields] =
	case fx (maybeToList $ lookup gfd_name fields) of
		(?Just x, _) -> (?Just (FIELD x), xs)
		_            -> (?None, xs)
gLSPJSONDecode{|FIELD|} _ xs = (?None, xs)

gLSPJSONEncode{|(?)|} fx (?Just x) = fx x
gLSPJSONEncode{|(?)|} fx ?None = [JSONNull]
derive gLSPJSONEncode Either
gLSPJSONEncode{|JSONNode|} node = [node]

gLSPJSONDecode{|(?)|} fx [JSONNull: xs] = (?Just ?None, xs)
gLSPJSONDecode{|(?)|} fx []             = (?Just ?None, []) // Interpret absentness as ?None
gLSPJSONDecode{|(?)|} fx xs = case fx xs of
	(?Just x, xs) -> (?Just (?Just x), xs)
	_ -> (?None, xs)
derive gLSPJSONDecode Either
gLSPJSONDecode{|JSONNode|} [node:xs] = (?Just node, xs)
gLSPJSONDecode{|JSONNode|} xs = (?None, xs)

gLSPJSONEncode{|[!]|} fx xs = [JSONArray (flatten [fx x \\ x <|- xs])]

gLSPJSONDecode{|[!]|} fx l=:[JSONArray items:xs]
	= case decodeItems fx items of
		(?Just x) -> (?Just x, xs)
		_         -> (?None, l)
gLSPJSONDecode{|[!]|} fx l = (?None, l)

gLSPJSONEncode{|[!!]|} fx xs = [JSONArray (flatten [fx x \\ x <|- xs])]

gLSPJSONDecode{|[!!]|} fx l=:[JSONArray items:xs]
	= case decodeItems fx items of
		(?Just x) -> (?Just x, xs)
		_         -> (?None, l)
gLSPJSONDecode{|[!!]|} fx l = (?None, l)

gLSPJSONEncode{|URI|} uri = [JSONString $ toString uri]

gLSPJSONDecode{|URI|} [JSONString x: xs] = (parseURI x, xs)
gLSPJSONDecode{|URI|} xs                 = (?None, xs)

decodeItems :: !([JSONNode] -> (?a, [JSONNode])) ![JSONNode] -> ?(l a) | List l a
decodeItems fx []       = ?Just [|]
decodeItems fx [ox:oxs] = case fx [ox] of
	(?Just x, _) -> case decodeItems fx oxs of
		(?Just xs) -> ?Just [|x:xs]
		_          -> ?None
	_ -> ?None

serialize :: !a -> JSONNode | gLSPJSONEncode{|*|} a
serialize x = case gLSPJSONEncode{|*|} x of
	[node] -> node
	_ -> abort "LSP.Internal.serialize: Failed serialization"

deserialize :: !JSONNode -> a | gLSPJSONDecode{|*|} a
deserialize x = case gLSPJSONDecode{|*|} [x] of
	(?Just x, []) -> x
	_ -> abort "LSP.Internal.deserialize: Failed deserialization"
