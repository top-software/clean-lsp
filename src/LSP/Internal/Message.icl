implementation module LSP.Internal.Message

from Text import instance Text String, class Text(trim, indexOf)
import Data.Either
import Data.Functor
import Data.Maybe
import LSP
import LSP.Internal.Serialize
import LSP.NotificationMessage
import LSP.RequestMessage
import StdEnv
import Text.GenJSON

nextMessage` :: !*File -> (?JSONNode, *File)
nextMessage` io
# (mbLength, io) = nextLength io
| isNone mbLength = (?None, io)
# io = skipToContent io
# (content, io) = freads io (fromJust mbLength)
| size content == 0 = (?None, io) // we get an empty string when the pipe is closed
| otherwise = (?Just (fromString content), io)
where
	/**
	 * Looks for the "Content-Length" field and parses the result
	 */
	nextLength :: !*File -> (!?Int, !*File)
	nextLength io
	# (line, io) = freadline io
	| size line == 0 = (?None, io) // we get an empty string when the pipe is closed
	// Removes uniqueness from line
	# line = trim line
	# mbheader = parseHeader line
	| mbheader=:(?None) = nextLength io
	# (?Just (field, content)) = mbheader
	| field == "Content-Length"
		# l = toInt content
		= (?Just l, io)
	| otherwise = nextLength io
	where
		parseHeader :: !String -> ?(!String, !String)
		parseHeader header
		# index = indexOf ":" header
		| index < 1 = ?None
		# name = trim (header % (0, index - 1))
		# value = trim (header % (index + 1, size header))
		= ?Just (name, value)

	/**
	 * A line only containing a newline denotes the start of the content part of the message.
	 */
	skipToContent :: !*File -> *File
	skipToContent io
	# (line, io) = freadline io
	| line == "\r\n" || size line == 0 = io
	| otherwise = skipToContent io

nextMessage :: !*File -> (?(Either RequestMessage NotificationMessage), *File)
nextMessage io
# (mbJson, io) = nextMessage` io
// See Note [No Constructors], we can ask for an Either, even if the result is only one of the two.
= (deserialize <$> mbJson, io)

sendMessage :: !*File !a -> *File | gLSPJSONEncode{|*|} a
sendMessage io a
# json = serialize a
# json = insertJSONRPCHeader json
# message = toString json
# l = size message
# io = io <<< "Content-Length:" <<< l <<< "\r\n\r\n" <<< message
// TODO: Check
# (_, io) = fflush io
= io
where
	insertJSONRPCHeader :: !JSONNode -> JSONNode
	insertJSONRPCHeader (JSONObject fields) = JSONObject [("jsonrpc", JSONRaw "2.0"):fields]
	insertJSONRPCHeader json = json
