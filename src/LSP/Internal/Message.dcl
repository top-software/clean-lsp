definition module LSP.Internal.Message

// TODO: Should not be internal anymore

from Data.Either import :: Either
from LSP.Internal.Serialize import generic gLSPJSONEncode
from LSP.NotificationMessage import :: NotificationMessage
from LSP.RequestMessage import :: RequestMessage
from Text.GenJSON import :: JSONNode

nextMessage :: !*File -> (?(Either RequestMessage NotificationMessage), *File)

/**
 * This function will insert the json-rpc header if the resulting json is an Object
 */
sendMessage :: !*File !a -> *File | gLSPJSONEncode{|*|} a
