implementation module LSP.InitializeParams

import LSP.Internal.Serialize

derive gLSPJSONEncode InitializeParams, ClientInfo, WorkspaceFolder
derive gLSPJSONDecode InitializeParams, ClientInfo, WorkspaceFolder
