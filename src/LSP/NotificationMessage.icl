implementation module LSP.NotificationMessage

import Data.GenEq
import StdEnv
import Data.Functor, Data.Maybe
import Text.GenJSON
import LSP.Internal.Serialize
import LSP.PublishDiagnosticsParams

derive gLSPJSONEncode NotificationMessage
derive gLSPJSONDecode NotificationMessage

notificationMessage :: !String !(?params) -> NotificationMessage | gLSPJSONEncode{|*|} params
notificationMessage method params = {NotificationMessage| method = method, params = serialize <$> params}

notificationMessagePublishDiagnostics :: !PublishDiagnosticsParams -> NotificationMessage
notificationMessagePublishDiagnostics publishDiagnosticsParams =
	notificationMessage "textDocument/publishDiagnostics" (?Just publishDiagnosticsParams)

instance == NotificationMessage derive gEq
