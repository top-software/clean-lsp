definition module LSP.ResponseMessage

from Data.Either import :: Either
from LSP.Internal.Serialize import generic gLSPJSONEncode, generic gLSPJSONDecode
from StdOverloaded import class toInt, class fromInt
from Text.GenJSON import :: JSONNode
from LSP.RequestId import :: RequestId

//* A response to a `RequestMessage` sent from the server to the client.
:: ResponseMessage =
	{ id     :: !?RequestId     //* The request ID
	, result :: !?JSONNode      //* The result, `?None` in case of an error.
	, error  :: !?ResponseError //* The error, must be `?None` in case the result is present.
	}
derive gLSPJSONEncode ResponseMessage
derive gLSPJSONDecode ResponseMessage

:: ResponseError =
	{ errorCode :: !ErrorCode
	, message :: !String
	, data :: !?JSONNode
	}
derive gLSPJSONEncode ResponseError
derive gLSPJSONDecode ResponseError

:: ErrorCode =
	ParseError | InvalidRequest | MethodNotFound | InvalidParams | InternalError | ServerNotInitialized |
	UnknownErrorCode | ContentModified | RequestCancelled
derive gLSPJSONEncode ErrorCode
derive gLSPJSONDecode ErrorCode

instance toInt ErrorCode
instance fromInt ErrorCode

/**
 * Constructs a message given the ID and if it should be an error or result.
 */
responseMessage :: !(?RequestId) !(Either ResponseError a) -> ResponseMessage | gLSPJSONEncode{|*|} a
