implementation module LSP.Diagnostic

import StdEnv
import Data.Func
import Text.GenJSON
import LSP.Internal.Serialize
import LSP.Range, LSP.Location

derive gLSPJSONEncode Diagnostic
derive gLSPJSONDecode Diagnostic

instance toInt DiagnosticSeverity
where
	toInt Error = 1
	toInt Warning = 2
	toInt Information = 3
	toInt Hint = 4

instance fromInt DiagnosticSeverity
where
	fromInt 1 = Error
	fromInt 2 = Warning
	fromInt 3 = Information
	fromInt 4 = Hint
	fromInt i = abort $ "LSP.Diagnostic.toInt DiagnosticSeverity: Could not convert " +++ toString i +++ " to severity"

gLSPJSONEncode{|DiagnosticSeverity|} severity = [JSONInt $ toInt severity]

gLSPJSONDecode{|DiagnosticSeverity|} [JSONInt i: xs] = (?Just $ fromInt i, xs)
gLSPJSONDecode{|DiagnosticSeverity|} xs              = (?None,             xs)

instance toInt DiagnosticTag
where
	toInt Unnecessary = 1
	toInt Deprecated = 2

instance fromInt DiagnosticTag
where
	fromInt 1 = Unnecessary
	fromInt 2 = Deprecated
	fromInt i = abort $ "LSP.Diagnostic.toInt DiagnosticTag: Could not convert " +++ toString i +++ " to tag"

derive gLSPJSONEncode DiagnosticTag
derive gLSPJSONDecode DiagnosticTag

derive gLSPJSONEncode DiagnosticRelatedInformation
derive gLSPJSONDecode DiagnosticRelatedInformation
