implementation module LSP.MessageParams

import StdEnv
import Data.Func
import Text.GenJSON
import LSP.Internal.Serialize

derive gLSPJSONEncode MessageParams
gLSPJSONEncode{|MessageType|} x = [JSONInt $ toInt x]

instance toInt MessageType
where
	toInt Error = 1
	toInt Warning = 2
	toInt Info = 3
	toInt Log = 4
