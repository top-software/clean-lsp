definition module LSP.Diagnostic

from StdEnv import class toInt, class fromInt
from Data.Either import :: Either
from Text.GenJSON import :: JSONNode
from Text.URI import :: URI
from LSP.Internal.Serialize import generic gLSPJSONEncode, generic gLSPJSONDecode
from LSP.Range import :: Range
from LSP.Location import :: Location
from LSP.PublishDiagnosticsParams import :: PublishDiagnosticsParams

/**
 * Diagnostic such as compiler/linter errors and warnings. They always concern a single resource (e.g. document).
 */
:: Diagnostic =
	{ range :: !Range
	, severity :: !?DiagnosticSeverity
	// TODO: commented out as `code` is a Clean keyword and the field is not usable therefore
	//, code :: !?(Either Int String)
	, codeDescription :: ?CodeDescription
	, source :: !?String
	, message :: !String
	, tags :: ![!DiagnosticTag]
	, relatedInformation :: ![!DiagnosticRelatedInformation]
	, data :: !JSONNode
	}

derive gLSPJSONEncode Diagnostic
derive gLSPJSONDecode Diagnostic

:: DiagnosticSeverity = Error | Warning | Information | Hint
instance toInt DiagnosticSeverity
instance fromInt DiagnosticSeverity

:: DiagnosticTag = Unnecessary | Deprecated
instance toInt DiagnosticTag
instance fromInt DiagnosticTag

:: DiagnosticRelatedInformation =
	{ location :: !Location
	, message :: !String
	}

:: CodeDescription :== URI
