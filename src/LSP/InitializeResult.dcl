definition module LSP.InitializeResult

from LSP.Internal.Serialize import generic gLSPJSONEncode, generic gLSPJSONDecode
from LSP.ServerCapabilities import :: ServerCapabilities
from Text.GenJSON import :: JSONNode

:: InitializeResult =
	{ capabilities :: ServerCapabilities
	, serverInfo :: ?ServerInfo
	}
derive gLSPJSONEncode InitializeResult
derive gLSPJSONDecode InitializeResult

:: ServerInfo =
	{ name :: String
	, version :: ?String
	}
derive gLSPJSONEncode ServerInfo
derive gLSPJSONDecode ServerInfo

/**
 * @param Name
 * @param Version
 */
initializeResult :: !String !String !ServerCapabilities -> InitializeResult
