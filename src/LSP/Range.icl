implementation module LSP.Range

import LSP.Internal.Serialize
import LSP.Position

derive gLSPJSONEncode Range
derive gLSPJSONDecode Range
