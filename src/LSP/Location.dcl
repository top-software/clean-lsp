definition module LSP.Location

from Text.GenJSON import :: JSONNode
from Text.URI import :: URI
from LSP.Internal.Serialize import generic gLSPJSONEncode, generic gLSPJSONDecode
from LSP.Range import :: Range

:: Location =
	{ uri :: !URI
	, range :: !Range
	}

derive gLSPJSONEncode Location
derive gLSPJSONDecode Location
