implementation module LSP.BasicTypes

import StdEnv
import Data.Func
import Text.GenJSON
import LSP.Internal.Serialize

:: UInt =: UInt Int

gLSPJSONEncode{|UInt|} (UInt x) = [JSONInt x]

gLSPJSONDecode{|UInt|} [JSONInt i:xs] = (?Just $ UInt i, xs)
gLSPJSONDecode{|UInt|} xs             = (?None,          xs)

uint :: !Int -> UInt
uint i
	// Could use max, but this can be faster
	| i < 0 = UInt 0
	| otherwise = UInt i
