definition module LSP.ShowMessageParams

from LSP.NotificationMessage import :: NotificationMessage
from LSP.MessageParams import :: MessageParams

:: ShowMessageParams :== MessageParams

/**
 * Constructs a "window/showMessage" notification Message from the provided params.
 */
showMessage :: !ShowMessageParams -> NotificationMessage
