implementation module LSP.RequestId

import LSP.Internal.Serialize

:: RequestId =: RequestId (Either Int String)

derive gLSPJSONEncode RequestId
derive gLSPJSONDecode RequestId
