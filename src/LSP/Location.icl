implementation module LSP.Location

import LSP.Internal.Serialize
import LSP.Range

derive gLSPJSONEncode Location
derive gLSPJSONDecode Location
