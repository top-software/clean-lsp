implementation module LSP.PublishDiagnosticsParams

import Text.URI
import LSP.Internal.Serialize
import LSP.Diagnostic

derive gLSPJSONEncode PublishDiagnosticsParams
derive gLSPJSONDecode PublishDiagnosticsParams
