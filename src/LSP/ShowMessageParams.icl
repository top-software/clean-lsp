implementation module LSP.ShowMessageParams

import LSP.MessageParams, LSP.NotificationMessage

showMessage :: !ShowMessageParams -> NotificationMessage
showMessage params = notificationMessage "window/showMessage" (?Just params)
