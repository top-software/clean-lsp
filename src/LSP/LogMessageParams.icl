implementation module LSP.LogMessageParams

import LSP.MessageParams, LSP.NotificationMessage

logMessage :: !LogMessageParams -> NotificationMessage
logMessage params = notificationMessage "window/logMessage" (?Just params)
