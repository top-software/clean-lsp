definition module LSP.BasicTypes

from Text.GenJSON import :: JSONNode
from LSP.Internal.Serialize import generic gLSPJSONEncode, generic gLSPJSONDecode

:: UInt =: UInt Int

derive gLSPJSONEncode UInt
derive gLSPJSONDecode UInt

/**
 * Converts an integer to an unsigned representation by clamping to 0.
 */
uint :: !Int -> UInt
