implementation module LSP.InitializeResult

import LSP.Internal.Serialize
import LSP.ServerCapabilities

derive gLSPJSONEncode InitializeResult
derive gLSPJSONDecode InitializeResult

derive gLSPJSONEncode ServerInfo
derive gLSPJSONDecode ServerInfo

initializeResult :: !String !String !ServerCapabilities -> InitializeResult
initializeResult name version sc =
	{ InitializeResult
	| capabilities = sc
	, serverInfo = ?Just
		{ ServerInfo
		| name = name
		, version = ?Just version
		}
	}
