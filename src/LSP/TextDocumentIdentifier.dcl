definition module LSP.TextDocumentIdentifier

from Text.URI import :: URI
from Text.GenJSON import :: JSONNode
from LSP.Internal.Serialize import generic gLSPJSONEncode, generic gLSPJSONDecode

//* Identifies a text document.
:: TextDocumentIdentifier =
	{ uri :: !URI //* The text document's URI.
	}

derive gLSPJSONEncode TextDocumentIdentifier
derive gLSPJSONDecode TextDocumentIdentifier
