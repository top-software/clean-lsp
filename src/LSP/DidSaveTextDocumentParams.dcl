definition module LSP.DidSaveTextDocumentParams

from Text.GenJSON import :: JSONNode
from LSP.Internal.Serialize import generic gLSPJSONEncode, generic gLSPJSONDecode
from LSP.TextDocumentIdentifier import :: TextDocumentIdentifier

//* Params for the event that a document was saved.
:: DidSaveTextDocumentParams =
	{ textDocument :: !TextDocumentIdentifier //* The saved document
	}

derive gLSPJSONEncode DidSaveTextDocumentParams
derive gLSPJSONDecode DidSaveTextDocumentParams
