definition module LSP.NotificationMessage

from LSP.Internal.Serialize import generic gLSPJSONEncode, generic gLSPJSONDecode
from LSP.PublishDiagnosticsParams import :: PublishDiagnosticsParams
from Text.GenJSON import :: JSONNode

:: NotificationMessage =
	{ method :: !String
	, params :: !?JSONNode
	}
derive gLSPJSONEncode NotificationMessage
derive gLSPJSONDecode NotificationMessage

//* A notification message with given method and optional parameter.
notificationMessage :: !String !(?params) -> NotificationMessage | gLSPJSONEncode{|*|} params

/**
 * A notification message causing diagnostics to be published to the client.
 *
 * @param The parameters required for publishing diagnostics to the client.
 * @result A notifcation message sending diagnostics to the client.
 */
notificationMessagePublishDiagnostics :: !PublishDiagnosticsParams -> NotificationMessage

instance == NotificationMessage
