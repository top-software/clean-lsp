definition module LSP.RequestMessage

from Data.Either import :: Either
from LSP.Internal.Serialize import generic gLSPJSONEncode, generic gLSPJSONDecode
from Text.GenJSON import :: JSONNode
from LSP.RequestId import :: RequestId

:: RequestMessage =
	{ id     :: !RequestId
	, method :: !String
	, params :: !?JSONNode
	}
derive gLSPJSONEncode RequestMessage
derive gLSPJSONDecode RequestMessage
