definition module LSP.DidOpenTextDocumentParams

from Text.GenJSON import :: JSONNode
from LSP.Internal.Serialize import generic gLSPJSONEncode, generic gLSPJSONDecode
from LSP.TextDocumentIdentifier import :: TextDocumentIdentifier

//* Params for the event that a document was opened.
:: DidOpenTextDocumentParams =
	{ textDocument :: !TextDocumentIdentifier //* The opened document.
	}

derive gLSPJSONEncode DidOpenTextDocumentParams
derive gLSPJSONDecode DidOpenTextDocumentParams
