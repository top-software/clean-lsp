definition module LSP.LogMessageParams

from LSP.NotificationMessage import :: NotificationMessage
from LSP.MessageParams import :: MessageParams

:: LogMessageParams :== MessageParams

/**
 * Constructs a "window/logMessage" notification Message from the provided params.
 */
logMessage :: !LogMessageParams -> NotificationMessage
