definition module LSP.Method.Progress

from Data.Either import :: Either

:: ProgressToken :== Either Int String

:: ProgressParams a =
	{ token :: !ProgressToken
	, value :: !a
	}
