implementation module LSP

import StdEnv
import StdMaybe
import StdOverloadedList
import Data.Either
import Data.Error
import Data.Func
import LSP.Internal.Message
import LSP.Internal.Serialize
import LSP.InitializeParams
import LSP.InitializeResult
import LSP.NotificationMessage
import LSP.ResponseMessage
import LSP.RequestMessage
import LSP.ServerCapabilities

serve :: !ServerCapabilities !(LanguageServer s) !*World -> *World
serve capabilities ls w
# (mbState, io, w) = initialize capabilities ls.onInitialize w
| isError mbState
	# (_, w) = fclose io w
	= w
# (io, w) = mainLoop ls (fromOk mbState) io w
# (_, w) = fclose io w
= w

/**
 * Performs the handshake, including sending the servercapabilities to the client
 */
initialize
	:: !ServerCapabilities !(InitializeParams *World -> (MaybeError String s, *World)) !*World
	-> (!MaybeError String s, !*File, !*World)
initialize capabilities onInitialize world
# (cs, world) = stdio world
# (id, params, cs) = expectInitialize cs
# (mbState, world) = onInitialize params world
| isError mbState
	# cs = sendResponseMessage (?Just (fromError mbState)) id cs
	= (mbState, cs, world)
# cs = sendResponseMessage ?None id cs
# cs = expectInitializeNotification cs
= (mbState, cs, world)
where
	expectInitialize :: !*File -> (!RequestId, !InitializeParams, !*File)
	expectInitialize cs = case nextMessage cs of
		(?Just (Left request), cs) | request.RequestMessage.method == "initialize" ->
			(request.RequestMessage.id, deserialize $ fromJust request.RequestMessage.params, cs)
		_ ->
			abort "LSP.initialize: Dit not receive initialize message.\n"

	sendResponseMessage :: !(?String) !RequestId !*File -> *File
	sendResponseMessage mbError id cs = sendMessage cs $ responseMessage (?Just id) message
	where
		message = case mbError of
			?None -> Right $ initializeResult "Eastwood" "WIP" capabilities
			?Just error -> Left
				{ errorCode = UnknownErrorCode
				, message = error
				, data = ?None
				}

	expectInitializeNotification :: !*File -> *File
	expectInitializeNotification cs = case nextMessage cs of
		(?Just (Right notification), cs) | notification.NotificationMessage.method == "initialized" ->
			cs
		_ ->
			abort "LSP.initialize: Did not receive initialized notification.\n"

/**
 * Performs the patternmaching on the received messages, calls the language server functions, and sends the result (if
 * any).
 */
mainLoop :: !(LanguageServer s) !s !*File !*World -> (!*File, !*World)
mainLoop ls s io world
# (mbMessage, io) = nextMessage io
| isNone mbMessage = (io, world)
# message = fromJust mbMessage
| isShutdownMessage message = (shutdown (fromLeft undef message) io, world)
# (s, io, world) = case message of
	Left request       = handleRequest request s io world
	Right notification = handleNotification notification s io world
= mainLoop ls s io world
where
	handleRequest request s io world
	# (response, s, world) = ls.onRequest request s world
	# io = sendMessage io response
	= (s, io, world)

	handleNotification notification s io world
	# (notifications, s, world) = ls.onNotification notification s world
	# io = Foldl sendMessage io notifications
	= (s, io, world)

	isShutdownMessage :: !(Either RequestMessage a) -> Bool
	isShutdownMessage (Left { RequestMessage | method = "shutdown" }) = True
	isShutdownMessage _ = False

	/**
	 * After a shutdown message, we must reply with an acknowledgement
	 */
	shutdown :: !RequestMessage !*File -> *File
	shutdown { RequestMessage | id } io = sendMessage io shutdownResponse
	where
		shutdownResponse :: ResponseMessage
		shutdownResponse =
			{ ResponseMessage
			| id = ?Just id
			, result = ?None
			, error = ?None
			}
