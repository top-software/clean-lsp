definition module LSP

from Data.Error import :: MaybeError
from LSP.InitializeParams import :: InitializeParams
from LSP.NotificationMessage import :: NotificationMessage
from LSP.RequestMessage import :: RequestMessage
from LSP.ResponseMessage import :: ResponseMessage
from LSP.ServerCapabilities import :: ServerCapabilities

/**
 * For now we implement only ways for the language server to respond to requests and update an internal state on
 * notification.
 *
 * onRequest is called whenever the server recieves a RequestMessage from the client. The server MUST respond with an
 * appropriate response.
 */
:: LanguageServer s =
	{ onInitialize :: !InitializeParams *World -> *(MaybeError String s, *World)
	  //* Handler for the initialize request, to produce the initial state.
	  //* When the handler returns an `Error`, the initialization is aborted.
	, onRequest :: !RequestMessage s *World -> *(ResponseMessage, s, *World)
	  //* Handler for request message; the server has to respond.
	, onNotification :: !NotificationMessage s *World -> *([!NotificationMessage], s, *World)
	  //* Handler for notification message;
	  //* the server can send notification messages back as reaction to a notification.
	}

/**
 * Performs the LSP handshake, calls the desired functions from the LanguageServer, and handles the shutdown
 * procedure.
 */
serve :: !ServerCapabilities !(LanguageServer s) !*World -> *World
