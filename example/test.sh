#!/usr/bin/env bash

# Send initialize request
echo -e -n 'Content-Length: 2688\r\n'
echo -e -n '\r\n'
echo -e -n '{"id": 1, "jsonrpc": "2.0", "method": "initialize", "params": {"initializationOptions": {}, "rootUri": "file:///home/erin/Projects/clean-lsp", "workspaceFolders": [{"uri": "file:///home/erin/Projects/clean-lsp", "name": "/home/erin/Projects/clean-lsp"}], "rootPath": "/home/erin/Projects/clean-lsp", "clientInfo": {"version": "0.6.0", "name": "Neovim"}, "processId": 55832, "trace": "off", "capabilities": {"callHierarchy": {"dynamicRegistration": false}, "window": {"showDocument": {"support": false}, "showMessage": {"messageActionItem": {"additionalPropertiesSupport": false}}, "workDoneProgress": true}, "workspace": {"workspaceFolders": true, "applyEdit": true, "workspaceEdit": {"resourceOperations": ["rename", "create", "delete"]}, "symbol": {"symbolKind": {"valueSet": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26]}, "dynamicRegistration": false, "hierarchicalWorkspaceSymbolSupport": true}, "configuration": true}, "textDocument": {"documentSymbol": {"symbolKind": {"valueSet": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26]}, "dynamicRegistration": false, "hierarchicalDocumentSymbolSupport": true}, "completion": {"completionItem": {"snippetSupport": false, "commitCharactersSupport": false, "preselectSupport": false, "deprecatedSupport": false, "documentationFormat": ["markdown", "plaintext"]}, "contextSupport": false, "dynamicRegistration": false, "completionItemKind": {"valueSet": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25]}}, "publishDiagnostics": {"relatedInformation": true, "tagSupport": {"valueSet": [1, 2]}}, "rename": {"prepareSupport": true, "dynamicRegistration": false}, "definition": {"linkSupport": true}, "references": {"dynamicRegistration": false}, "codeAction": {"codeActionLiteralSupport": {"codeActionKind": {"valueSet": ["", "Empty", "QuickFix", "Refactor", "RefactorExtract", "RefactorInline", "RefactorRewrite", "Source", "SourceOrganizeImports", "quickfix", "refactor", "refactor.extract", "refactor.inline", "refactor.rewrite", "source", "source.organizeImports"]}}, "dynamicRegistration": false}, "declaration": {"linkSupport": true}, "signatureHelp": {"signatureInformation": {"documentationFormat": ["markdown", "plaintext"]}, "dynamicRegistration": false}, "documentHighlight": {"dynamicRegistration": false}, "hover": {"dynamicRegistration": false, "contentFormat": ["markdown", "plaintext"]}, "synchronization": {"didSave": true, "willSaveWaitUntil": false, "willSave": false, "dynamicRegistration": false}, "implementation": {"linkSupport": true}, "typeDefinition": {"linkSupport": true}}}}}'

# Received initialize response

# Send initialized notification
echo -e -n 'Content-Length: 57\r\n'
echo -e -n '\r\n'
echo -e -n '{"jsonrpc": "2.0", "method": "initialized", "params": {}}'

# Send our first request
echo -e -n 'Content-Length: 157\r\n'
echo -e -n '\r\n'
echo -e -n '{"jsonrpc": "2.0", "method": "textDocument/definition", "id": 2, "params": {"textDocument": {"uri": "file://temp"}, "position": {"line": 1, "character": 1}}}'

# Receive error message
