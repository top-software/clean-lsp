module example

import Data.Either
import LSP
import LSP.ResponseMessage
import LSP.RequestMessage
import LSP.ServerCapabilities
import StdEnv
import Data.Error

exampleLanguageServer :: LanguageServer ()
exampleLanguageServer =
	{ LanguageServer
	| onInitialize = \_ world -> (Ok (), world)
	, onRequest = onRequest
	// One must not reply to Notifications
	, onNotification = (\_ _ world -> ([!], (), world))
	}

onRequest :: !RequestMessage !() !*World -> (ResponseMessage, (), *World)
onRequest {RequestMessage | id} _ world = (errorResponse id, (), world)
where
	errorResponse :: !RequestId -> ResponseMessage
	errorResponse id =
		{ ResponseMessage
		| id = ?Just id
		, result = ?None
		, error = ?Just
			{ ResponseError
			| errorCode = InternalError
			, message = "This language server is still work in progress."
			, data = ?None
			}
		}

myCapabilities :: ServerCapabilities
myCapabilities =
	{ ServerCapabilities
	| textDocumentSync = {openClose = False, save = False}
	, declarationProvider = False
	, definitionProvider = False
	}

Start :: !*World -> *World
Start w = serve myCapabilities exampleLanguageServer w
