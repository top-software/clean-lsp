# Installation
## Neovim
The configuration below assumes you have neovim 0.5 or greater with lsp-config installed. Simply add the following to
your vimrc:
```lua
lua << EOF
  local lspconfig = require'lspconfig'
  local configs = require'lspconfig/configs'
  if not lspconfig.example_lsp then
    configs.example_lsp = {
      default_config = {
        cmd = {'PATH_TO_YOUR_EXAMPLE_BINARY'};
        filetypes = {'clean'};
        root_dir = function(fname)
          return lspconfig.util.find_git_ancestor(fname) or vim.loop.os_homedir()
        end;
        settings = {};
      };
    }
  end
  lspconfig.example_lsp.setup{}
EOF
```
