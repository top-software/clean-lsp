# The Language Server Protocol (LSP)
For the current specification of the LSP see [Microsoft's
documentation](https://microsoft.github.io/language-server-protocol/specifications/specification-current/). A concise
overview of the protocol follows.

## Terminology
The editor/IDE is considered to be the "client". The program using this library is considered to be the "server".

## Message
A LSP message always contains a header with 1 or 2 fields (1 being required) to indicate the length of the content part
(required) and
the encoding used (optional, assumed to be `utf-8`). The content of the message MUST be in JSON format, and contain
WILL always contain the `jsonrpc` field. At the time of writing, this field MUST always be set to "2.0". Depending on
the kind of message (see below), additional fields are added.

## Messages
The LSP consists of only three kinds of messages:
1. Request Message (Send by client): The request message asks the server to perform some kind of action or ask for some
   kind of information. The server MUST respond to these requests with a response.
2. Response Message (Send by server): The answer to a request message. Can contain the desired result or optionally
   contains an error (including an error code and message). Even when a request does not result in anything, a response
   MUST still be send. In such a case the `result` field may be empty/null. The `id` of the response MUST be the id
   corresponding to the id of the request message.
3. Notification Message (Send by both client and server): Notifications messages are messages send from either client
   or server that MUST not be met with a response. Several notifications are built into the LSP. Examples are the
   `didSave` notification with which the client indicates it has saved the document, and the `publishDiagnostics`
   notification which allows the server to send `Diagnostics` to the client.

## Methods
Request and Notifications messages include a `method` field to indicate the desired effect. During initialization the
client and server negotiate which methods both parties support. The LSP defines 2 special methods (denoted by a `$`)
that certain language servers CANNOT support due to technical constraints. The `clean-lsp` is a library that WILL NOT
provide support for methods starting with `$` due to such technical constraints.

## Request Messages
```clean
:: RequestMessage =
	{ id :: !Either Int String
	, method :: !String
	, params :: !?JSONNode
	}
```
The above type is the implementation of the Request message in `clean-lsp`, notably missing is the aforementioned
`jsonrpc` field. This field is injected during serialization by the library as this field MUST always be "2.0". The
`id` field contains a unique id that the server should include in its Response message. The `method` contains the
desired information or side-effect. See the aforementioned LSP Specification for a list of `methods` the server can
implement. `params` is a field that typically contains a `JSONObject` although this can vary depending on the `method`.
This field contains all additional information the server should need to process the request.

## Response Messages
```clean
:: ResponseMessage =
	{ id :: !?(Either Int String)
	, result :: !?JSONNode
	, error :: ?ResponseError
	}
```
Responses are sent exclusively from the server to the client. The `id` field MUST refer to the Request this Response is
a response to. The specification dictates that the `id` field can also be `null` although it is unclear to me why this
is allowed. The `result` is the response to the requested `method` and thus depends on the method. This field MUST
exist if the `method` was successful, if not, the field MUST NOT exist. The final `error` field MUST exist in case of a
failure. Please refer to either `LSP.ResponseMessage` or the specification to see what structure this field must have.

## Notification Messages
```clean
:: NotificationMessage =
	{ method :: !String
	, params :: !?JSONNode
	}
```
Notifications messages are similar to Request messages with the notable difference that Notification messages MUST not
be answered and therefore do not contain an `id` field. Otherwise, these messages function nearly identical.

# Clean-lsp
Clean-lsp models the aforementioned messages as described above. Much work remains to be done, implementing types for
all methods, enabling IO for LSP functions and changing the type of the Request, Response and Notifications messages to
be less hacky.

## Module layout
### `LSP`
Contains all general functions pertaining to the LSP, mostly deals with the logic of the server. In the future, we
might want to consider to which extend we wish to abstract this module. Currently, this module provides a relatively
low access, for example requiring the user to construct the Response JSON themself.

### `LSP.*`
Contains all types that can be used when constructing messages. All types should be in a distinct module, perhaps with
some smart constructors or utility functions.

### `LSP.Internal.*`
Contains all modules that we do not intent the user of the library to use.
